/*
MUTAnimator is simple program for creating animations, developed for MUTA.
Copyright (C) 2017 Jerba

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <string>

struct Button
{
public:
	Button(int x, int y, int w, int h)
	{
		position_x = x;
		position_y = y;
		width = w;
		height = h;
	}

	void Free()
	{
		SDL_free(&buttonrect);
		SDL_free(&textrect);
		SDL_DestroyTexture(textures[0]);
		SDL_DestroyTexture(textures[1]);
		SDL_DestroyTexture(textures[2]);
		SDL_DestroyTexture(textures[3]);
	}

	void Initialize(char text[], TTF_Font *font, SDL_Renderer *renderer)
	{
		SDL_Color color{ 255, 255, 255 };
		SDL_Surface* surfaceMessage = TTF_RenderText_Solid(font, text, color);

		textures[0] = SDL_CreateTextureFromSurface(renderer, surfaceMessage); //now you can convert it into a texture

		SDL_FreeSurface(surfaceMessage);
		color = { 180, 180, 180 };
		surfaceMessage = TTF_RenderText_Solid(font, text, color);

		textures[1] = SDL_CreateTextureFromSurface(renderer, surfaceMessage); //now you can convert it into a texture

		SDL_FreeSurface(surfaceMessage);
		color = { 255, 255, 0 };
		surfaceMessage = TTF_RenderText_Solid(font, text, color);

		textures[2] = SDL_CreateTextureFromSurface(renderer, surfaceMessage); //now you can convert it into a texture

		SDL_FreeSurface(surfaceMessage);
		color = { 200, 200, 0 };
		surfaceMessage = TTF_RenderText_Solid(font, text, color);

		textures[3] = SDL_CreateTextureFromSurface(renderer, surfaceMessage); //now you can convert it into a texture

		buttonrect = { position_x, position_y, width, height };

		int textW = 0, textH = 0;
		SDL_QueryTexture(textures[0], NULL, NULL, &textW, &textH);

		textrect = { position_x + width / 2 - textW / 2, position_y + height / 2 - textH / 2, textW, textH };

		if (textrect.w > buttonrect.w - 4)
		{
			float multiplier = (float)textrect.w / ((float)buttonrect.w - 4);
			textW = (float)textrect.w / multiplier;
			textH = (float)textrect.h / multiplier;

			textrect.x = position_x + width / 2 - textW / 2;
			textrect.y = position_y + height / 2 - textH / 2;
			textrect.w = textW;
			textrect.h = textH;
		}

		SDL_FreeSurface(surfaceMessage);
	}

	int position_x, position_y, width, height;

	bool Pressed()
	{
		return pressed != pressed_old;
	}

	bool Released()
	{
		return released != released_old;
	}

	bool Held()
	{
		return held;
	}

	int HeldTimer()
	{
		return heldTimer;
	}

	bool Hovering()
	{
		return hovering;
	}

	void Update()
	{
		pressed_old = pressed;
		released_old = released;

		int mouseX = 0, mouseY = 0;

		SDL_GetMouseState(&mouseX, &mouseY);

		if (mouseX >= position_x && mouseX <= position_x + width && mouseY >= position_y && mouseY <= position_y + height)
		{
			hovering = true;

			if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT))
			{
				if (!held) pressed = true;
				released = released_old = false;
				held = true;
			}
			else
			{
				if (held) released = true;
				pressed = pressed_old = false;
				held = false;
			}
		}
		else
		{
			pressed = pressed_old = false;
			released = released_old = false;
			held = false;
			hovering = false;
		}

		if (held) { ++heldTimer; }
		else { heldTimer = 0; }
	}

	void Render(SDL_Renderer *renderer)
	{
		SDL_SetRenderDrawColor(renderer, 0, 0, 255, 100);

		SDL_RenderFillRect(renderer, &buttonrect);

		if (Pressed() || Held())
		{
			SDL_RenderCopy(renderer, textures[2], NULL, &textrect);
		}
		else if (Released())
		{
			SDL_RenderCopy(renderer, textures[3], NULL, &textrect);
		}
		else if (Hovering())
		{
			SDL_RenderCopy(renderer, textures[1], NULL, &textrect);
		}
		else
		{
			SDL_RenderCopy(renderer, textures[0], NULL, &textrect);
		}
	}

private:
	bool pressed = false; bool pressed_old = false;
	bool released = false; bool released_old = false;
	bool held = false;
	bool hovering = false;
	int heldTimer = 0;

	SDL_Rect buttonrect;
	SDL_Rect textrect;

	SDL_Texture *textures[4];
};
