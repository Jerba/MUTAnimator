/*
MUTAnimator is simple program for creating animations, developed for MUTA.
Copyright (C) 2017 Jerba

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define _CRT_SECURE_NO_WARNINGS

#ifdef _WIN32
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
#elif defined(__GNUC__)
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#endif
#include <iostream>
#include <vector>
#include "button.h"
#include "animation.h"
#include <string.h>
#include <unordered_map>

int SaveAnimation();
int LoadAnimation(SDL_Renderer *renderer);
int LoadAnimation(SDL_Renderer *renderer, char* droppedPath);

float zoom = 1.0f;

int startX = 0, startY = 0;
SDL_Rect MouseRect{ -64, -64, 1, 1 };
bool PlacingRect = false;

Button* buttons[31];
const int buttonsSize = 31;
const float framerate = 1.0f / 60.0f;
const float oneframe = 0.0166667f;
float frametimer = 0.0f;
char* InputText = "Text";

int used_textures_index = 0;
SDL_Texture *used_textures[256];
std::unordered_map<std::string, int> texture_map;

Animation *isometricAnim[8];

char* FileToLoad;

int curFrame = 0;
int curClip = 0;

bool SelectingSource = false;
bool MutanimMode = true;

float viewOffsetX;
float viewOffsetY;

int select_off_x = 0, select_off_y = 0;
int select_off_anchor_x = 0, select_off_anchor_y;

bool WordContainedIn(const char* word, const char* sentence)
{
	return strstr(sentence, word);
} //sentence.find(word) != std::string::npos; }

void PlaceMouseRect()
{
	int oldX = startX;
	int oldY = startY;

	int mouseX = 0; int mouseY = 0;
	SDL_GetMouseState(&mouseX, &mouseY);

	if (mouseX < startX)
	{
		startX = mouseX;
		mouseX = oldX;
	}

	if (mouseY < startY)
	{
		startY = mouseY;
		mouseY = oldY;
	}

	int width = startX - mouseX;
	if (width < 0) width *= -1;

	int height = startY - mouseY;
	if (height < 0) height *= -1;

	MouseRect.x = startX;
	MouseRect.y = startY;
	MouseRect.w = width;
	MouseRect.h = height;

	startX = oldX;
	startY = oldY;
}

void KeyboardAdjust()
{
	int w, h;
	AnimFrame *frame = isometricAnim[curClip]->GetFrameAt(curFrame);

	if (!frame)
		return;

	SDL_QueryTexture(frame->texture, NULL, NULL, &w, &h);

	const Uint8 *state = SDL_GetKeyboardState(NULL);

	if (state[SDL_SCANCODE_W])
	{
		viewOffsetY--;
	}
	else if (state[SDL_SCANCODE_S])
	{
		viewOffsetY++;
	}
	if (state[SDL_SCANCODE_A])
	{
		viewOffsetX--;
	}
	else if (state[SDL_SCANCODE_D])
	{
		viewOffsetX++;
	}
	if (state[SDL_SCANCODE_Q] && state[SDL_SCANCODE_E])
	{
		viewOffsetX = viewOffsetY = 0;
	}

	if (state[SDL_SCANCODE_KP_PLUS])
	{
		zoom += 0.01f;
		if (zoom > 10)
		{
			zoom = 10;
		}
	}
	else if (state[SDL_SCANCODE_KP_MINUS])
	{
		zoom -= 0.01f;
		if (zoom < 0.5)
		{
			zoom = 0.5f;
		}
	}

	if (state[SDL_SCANCODE_LSHIFT])
	{
		if (state[SDL_SCANCODE_UP])
		{
			frame->sourceRect.h--;
		}
		if (state[SDL_SCANCODE_DOWN])
		{
			frame->sourceRect.h++;
		}

		if (state[SDL_SCANCODE_RIGHT])
		{
			frame->sourceRect.w++;
		}
		if (state[SDL_SCANCODE_LEFT])
		{
			frame->sourceRect.w--;
		}
	}
	else
	{
		if (state[SDL_SCANCODE_UP])
		{
			frame->sourceRect.y--;
		}
		if (state[SDL_SCANCODE_DOWN])
		{
			frame->sourceRect.y++;
		}
		if (state[SDL_SCANCODE_RIGHT])
		{
			frame->sourceRect.x++;
		}
		if (state[SDL_SCANCODE_LEFT])
		{
			frame->sourceRect.x--;
		}
	}

	if (frame->sourceRect.h <= 0)
	{
		frame->sourceRect.h = 0;
	}

	if (frame->sourceRect.h > h)
	{
		frame->sourceRect.h = h;
	}

	if (frame->sourceRect.w > w)
	{
		frame->sourceRect.w = w;
	}

	if (frame->sourceRect.w <= 0)
	{
		frame->sourceRect.w = 0;
	}

	if (frame->sourceRect.y <= 0)
	{
		frame->sourceRect.y = 0;
	}

	if (frame->sourceRect.y > h - frame->sourceRect.h)
	{
		frame->sourceRect.y = h - frame->sourceRect.h;
	}

	if (frame->sourceRect.x > w - frame->sourceRect.w)
	{
		frame->sourceRect.x = w - frame->sourceRect.w;
	}

	if (frame->sourceRect.x < 0)
	{
		frame->sourceRect.x = 0;
	}
}

bool text_is_legit(char* text_to_check)
{
	char* c = text_to_check;

	char* legal_chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890_";

	while (*c)
	{
		if (!strchr(legal_chars, *c))
		{
			return false;
		}

		++c;
	}

	return true;
}

char* append_char(char* array, char a)
{
	size_t len = strlen(array);

	char* ret = new char[len + 2];

	strcpy(ret, array);
	ret[len] = a;
	ret[len + 1] = '\0';

	return ret;
}

int main(int argc, char* args[])
{
	Uint64 NOW = SDL_GetPerformanceCounter();
	Uint64 LAST = 0;
	double deltaTime = 0;

	bool quit = false;
	SDL_Event event;

	SDL_Init(SDL_INIT_VIDEO);
	IMG_Init(IMG_INIT_PNG);
	TTF_Init();

	SDL_Window * window = SDL_CreateWindow("Jerba's Animation Tool - DO NOT look at the code",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1600,
		900, 0);
	SDL_Renderer * renderer = SDL_CreateRenderer(window, -1, 0);

	TTF_Font *Font = TTF_OpenFont("PressStart2P.ttf", 20);

	float seconds = 0;

	isometricAnim[8];

	for (int i = 0; i < 8; ++i)
	{
		isometricAnim[i] = 0;
		Animation *isoAnim = new Animation();
		isoAnim->Initialize();

		isometricAnim[i] = isoAnim;
	}

	SDL_Surface * image = NULL;
	image = IMG_Load("idle.png");

	if (image == NULL)
	{
		SDL_Color color{ 255, 255, 255 };
		image = TTF_RenderText_Solid(Font, "Missing Image", color);
	}

	used_textures_index = 0;
	texture_map["DEFAULT"] = used_textures_index;

	SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer, image);
	used_textures[used_textures_index] = texture;

	used_textures_index++;

	for (int i = 0; i < 8; ++i)
	{
		AnimFrame *frame = (AnimFrame*)malloc(sizeof(AnimFrame));
		frame->filename = "default";
		frame->texture = texture;
		frame->sourceRect = { 0, 0, 8, 8 };
		frame->offset_x = 0;
		frame->offset_y = 0;
		frame->frameDuration = 0.0166667f * 6;
		frame->timer = 0;

		int w, h;
		SDL_QueryTexture(texture, NULL, NULL, &w, &h);

		frame->sourceRect.w = w;
		frame->sourceRect.h = h;

		isometricAnim[i]->Empty();
		isometricAnim[i]->Add(frame, true);
	}

	//Uint32 unityblue = SDL_MapRGB(image->format, 49, 77, 121);
	//Uint32 red = SDL_MapRGB(image->format, 255, 0, 0);
	//Uint32 green = SDL_MapRGB(image->format, 0, 255, 0);
	//Uint32 blue = SDL_MapRGB(image->format, 0, 0, 255);

	SDL_SetRenderDrawColor(renderer, 49, 77, 121, 255);
	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

	KeyboardAdjust();

	int buttonY = 16;
	int buttonW = 96;
	int buttonH = 48;
	int buttonSpace = 16;

	//Buttons move up down left right
	buttons[0] = new Button{ 1600 - buttonSpace - buttonW, buttonY, buttonW, buttonH };
	buttons[1] = new Button{ 1600 - buttonSpace * 2 - buttonW * 2, buttonY, buttonW, buttonH };
	buttons[2] = new Button{ 1600 - buttonSpace * 3 - buttonW * 3, buttonY, buttonW, buttonH };
	buttons[3] = new Button{ 1600 - buttonSpace * 4 - buttonW * 4, buttonY, buttonW, buttonH };

	buttonY += buttonH + buttonSpace;

	//Buttons size up down left right
	buttons[4] = new Button{ 1600 - buttonSpace - buttonW, buttonY, buttonW, buttonH };
	buttons[5] = new Button{ 1600 - buttonSpace * 2 - buttonW * 2, buttonY, buttonW, buttonH };
	buttons[6] = new Button{ 1600 - buttonSpace * 3 - buttonW * 3, buttonY, buttonW, buttonH };
	buttons[7] = new Button{ 1600 - buttonSpace * 4 - buttonW * 4, buttonY, buttonW, buttonH };

	buttonY += buttonH + buttonSpace;

	//Select and make new frame
	buttons[8] = new Button{ 1600 - buttonSpace - buttonW, buttonY, buttonW, buttonH };
	buttons[9] = new Button{ 1600 - buttonSpace * 2 - buttonW * 2, buttonY, buttonW, buttonH };
	buttons[10] = new Button{ 1600 - buttonSpace * 3 - buttonW * 3, buttonY, buttonW, buttonH };
	buttons[22] = new Button{ 1600 - buttonSpace * 4 - buttonW * 4, buttonY, buttonW, buttonH };

	buttonY += buttonH + buttonSpace;

	//Move frame backward and forward
	buttons[11] = new Button{ 1600 - buttonSpace - buttonW, buttonY, buttonW, buttonH };
	buttons[12] = new Button{ 1600 - buttonSpace * 2 - buttonW * 2, buttonY, buttonW, buttonH };
	buttons[25] = new Button{ 1600 - buttonSpace * 3 - buttonW * 3, buttonY, buttonW, buttonH };

	buttonY += buttonH + buttonSpace;

	//Offset buttons
	buttons[13] = new Button{ 1600 - buttonSpace - buttonW, buttonY, buttonW, buttonH };
	buttons[14] = new Button{ 1600 - buttonSpace * 2 - buttonW * 2, buttonY, buttonW, buttonH };
	buttons[15] = new Button{ 1600 - buttonSpace * 3 - buttonW * 3, buttonY, buttonW, buttonH };
	buttons[16] = new Button{ 1600 - buttonSpace * 4 - buttonW * 4, buttonY, buttonW, buttonH };

	buttonY += buttonH + buttonSpace;

	buttons[26] = new Button{ 1600 - buttonSpace - buttonW, buttonY, buttonW, buttonH };
	buttons[27] = new Button{ 1600 - buttonSpace * 2 - buttonW * 2, buttonY, buttonW, buttonH };
	buttons[28] = new Button{ 1600 - buttonSpace * 3 - buttonW * 3, buttonY, buttonW, buttonH };
	buttons[29] = new Button{ 1600 - buttonSpace * 4 - buttonW * 4, buttonY, buttonW, buttonH };

	buttonY += buttonH + buttonSpace;

	//Duration
	buttons[17] = new Button{ 1600 - buttonSpace - buttonW, buttonY, buttonW, buttonH };
	buttons[18] = new Button{ 1600 - buttonSpace * 2 - buttonW * 2, buttonY, buttonW, buttonH };

	buttonY += buttonH + buttonSpace;

	//Duration
	buttons[19] = new Button{ 1600 - buttonSpace - buttonW, buttonY, buttonW, buttonH };
	buttons[20] = new Button{ 1600 - buttonSpace * 2 - buttonW * 2, buttonY, buttonW, buttonH };

	buttonY += buttonH + buttonSpace;
	buttonY += buttonH + buttonSpace;

	//Save
	buttons[21] = new Button{ 1600 - buttonSpace - buttonW, buttonY, buttonW, buttonH };
	buttons[30] = new Button{ 1600 - buttonSpace * 2 - buttonW * 2, buttonY, buttonW, buttonH };

	buttonY += buttonH + buttonSpace;

	//Clips
	buttons[23] = new Button{ 1600 - buttonSpace - buttonW, buttonY, buttonW, buttonH };
	buttons[24] = new Button{ 1600 - buttonSpace * 2 - buttonW * 2, buttonY, buttonW, buttonH };


	buttons[0]->Initialize("Up", Font, renderer);
	buttons[1]->Initialize("Down", Font, renderer);
	buttons[2]->Initialize("Left", Font, renderer);
	buttons[3]->Initialize("Right", Font, renderer);

	buttons[4]->Initialize("Height-", Font, renderer);
	buttons[5]->Initialize("Height+", Font, renderer);
	buttons[6]->Initialize("Width-", Font, renderer);
	buttons[7]->Initialize("Width+", Font, renderer);

	buttons[8]->Initialize("New Frame", Font, renderer);
	buttons[9]->Initialize("Next Frame", Font, renderer);
	buttons[10]->Initialize("Prev Frame", Font, renderer);

	buttons[11]->Initialize("Move Right", Font, renderer);
	buttons[12]->Initialize("Move Left", Font, renderer);

	buttons[13]->Initialize("Off Y-", Font, renderer);
	buttons[14]->Initialize("Off Y+", Font, renderer);
	buttons[15]->Initialize("Off X-", Font, renderer);
	buttons[16]->Initialize("Off X+", Font, renderer);

	buttons[17]->Initialize("Zoom Out", Font, renderer);
	buttons[18]->Initialize("Zoom In", Font, renderer);

	buttons[19]->Initialize("Dur -", Font, renderer);
	buttons[20]->Initialize("Dur +", Font, renderer);

	buttons[21]->Initialize("Save", Font, renderer);
	buttons[22]->Initialize("Delete", Font, renderer);
	buttons[30]->Initialize("Mode", Font, renderer);

	buttons[23]->Initialize("Next Clip", Font, renderer);
	buttons[24]->Initialize("Prev Clip", Font, renderer);

	buttons[25]->Initialize("Selection", Font, renderer);

	buttons[26]->Initialize("Off All Y-", Font, renderer);
	buttons[27]->Initialize("Off All Y+", Font, renderer);
	buttons[28]->Initialize("Off All X-", Font, renderer);
	buttons[29]->Initialize("Off All x+", Font, renderer);

	MutanimMode = true;

	SDL_Color memecolor{ 255, 255, 255, 0 };
	SDL_Surface *memesurface = TTF_RenderText_Blended(Font, "For un-educational use only", memecolor);
	SDL_Texture *memetexture = SDL_CreateTextureFromSurface(renderer, memesurface); //now you can convert it into a texture
	SDL_FreeSurface(memesurface);
	int memeW, memeH; SDL_QueryTexture(memetexture, NULL, NULL, &memeW, &memeH);
	memeW /= 2; memeH /= 2;
	SDL_Rect memerect{ 1600 - memeW, 900 - memeH, memeW, memeH };
	memesurface = NULL;

	char* dropped_filedir;

	SDL_EventState(SDL_DROPFILE, SDL_ENABLE);

	float editedFrameDurationOld = -1;
	int editedFrameIndexOld = -1;

	SDL_Color textColor{ 255, 255, 255 };
	SDL_Surface* CurrentFrameTextSurface = TTF_RenderText_Solid(Font, "Frame: ", textColor);
	SDL_Texture* CurrentFrameText = SDL_CreateTextureFromSurface(renderer, CurrentFrameTextSurface); //now you can convert it into a texture
	SDL_Rect CurrentFrameTextDest{ 0, 0, 144, 48 };

	SDL_Surface* CurrentFrameDurSurface = TTF_RenderText_Solid(Font, "Duration: ", textColor);
	SDL_Texture* CurrentFrameDur = SDL_CreateTextureFromSurface(renderer, CurrentFrameDurSurface); //now you can convert it into a texture
	SDL_Rect CurrentFrameDurDest{ 0, 0, 1, 1 };

	SDL_Surface* InputTextSurface = NULL;
	SDL_Texture* InputTextTexture = NULL; //now you can convert it into a texture

	SDL_Surface* SaveHowToSurface = TTF_RenderText_Solid(Font, "Enter name and press Enter to Save Animation or click 'Save' again to Cancel", textColor);;
	SDL_Texture* SaveHowToTexture = SDL_CreateTextureFromSurface(renderer, SaveHowToSurface); //now you can convert it into a texture

	SDL_Surface* CurClipSurface = TTF_RenderText_Solid(Font, "Clip: ", textColor);
	SDL_Texture* CurClipTexture = SDL_CreateTextureFromSurface(renderer, CurClipSurface); //now you can convert it into a texture
	SDL_Rect CurClipDest{ 0, 0, 1, 1 };

	SDL_Surface* MutanimModeSurface = TTF_RenderText_Solid(Font, "mutanim", textColor);
	SDL_Texture* MutanimModeTexture = SDL_CreateTextureFromSurface(renderer, MutanimModeSurface);
	SDL_Surface* AnimModeSurface	= TTF_RenderText_Solid(Font, "anim", textColor);
	SDL_Texture* AnimModeTexture	= SDL_CreateTextureFromSurface(renderer, AnimModeSurface);

	int mode_size = strlen(".mutanim");
	SDL_Rect MutanimModeDest{ 1360 - (20 * mode_size), 605,  (20 * mode_size), 25 };

	mode_size = strlen(".anim");
	SDL_Rect AnimModeDest{ 1360 - (20 * mode_size), 605,  (20 * mode_size), 25 };

	SDL_Rect HowToSaveRect{ 800 - ((16 * 76) / 2), 700, (16 * 76), 20 };


	SDL_Surface * tileSurf = NULL;
	tileSurf = IMG_Load("tile.png");

	if (tileSurf == NULL)
	{
		SDL_Color color{ 255, 255, 255 };
		tileSurf = TTF_RenderText_Solid(Font, "No Tile", color);
	}

	SDL_Texture * tileTex = SDL_CreateTextureFromSurface(renderer, tileSurf);
	SDL_FreeSurface(tileSurf);

	bool Saving = false;
	SelectingSource = false;
	bool ReleasedAfterStartingSelection = false;
	bool PlacedShit = false;

	float keyTimer = 0;

	while (!quit)
	{
		LAST = NOW;
		NOW = SDL_GetPerformanceCounter();

		deltaTime = ((NOW - LAST) * 1000 / (double)SDL_GetPerformanceFrequency());
		bool update = false;

		frametimer += deltaTime * 0.001f;
		if (keyTimer > 0) keyTimer -= deltaTime * 0.001f;

		if (frametimer >= framerate)
		{
			frametimer = 0;
			update = true;
		}

		seconds += deltaTime * 0.001f;

		isometricAnim[curClip]->Update(deltaTime * 0.001f);

		//printf("deltaT: %f, curT: %f\n", deltaTime, curTime);
		//printf("seconds: %f\n", seconds);

		SDL_PollEvent(&event);

		switch (event.type)
		{
		case SDL_QUIT:
			quit = true;
			break;
		case SDL_KEYDOWN:
			if (Saving)
			{
				int size = strlen(InputText);

				//printf("size: %d\n", size);

				if (event.key.keysym.sym == SDLK_BACKSPACE && size > 0)
				{
					if (keyTimer <= 0)
					{
						char* temp = (char*)malloc(strlen(InputText));

						for (int a = 0; a < size; ++a)
						{
							if (a == size - 1)
							{
								temp[a] = '\0';
							}
							else
							{
								temp[a] = InputText[a];
							}
						}

						InputText = temp;

						keyTimer = 0.1f;
					}
					//InputText.pop_back();
				}
				else if (event.key.keysym.sym == SDLK_RETURN)
				{
					if (keyTimer <= 0)
					{
						keyTimer = 0.15f;

						int size = strlen(InputText);

						if (size > 1)
						{
							Saving = false;
							SDL_StopTextInput();

							if (text_is_legit(InputText))
							{
								SaveAnimation();
							}
							else
							{
								printf("The text contains illegal characters\n");
							}
						}
						else
						{
							printf("Please give longer filename\n");
						}
					}
				}
			}
			break;
		case SDL_TEXTINPUT:
		{
			if (Saving)
			{
				int size = strlen(InputText);
				if (keyTimer <= 0 && size < 32)
				{
					if (text_is_legit(event.text.text))
					{
						InputText = append_char(InputText, event.text.text[0]);
						//InputText += event.text.text;
						keyTimer = 0.15f;
					}
					else
					{
						printf("The text contains illegal characters\n");
					}
				}
			}
		}
		break;
		case (SDL_DROPFILE):      // In case if dropped file
			dropped_filedir = event.drop.file;

			if (WordContainedIn(".png", dropped_filedir))
			{
			}
			else if (WordContainedIn(".jpg", dropped_filedir))
			{
			}
			else if (WordContainedIn(".mutanim", dropped_filedir) || WordContainedIn(".anim", dropped_filedir))
			{
				LoadAnimation(renderer, dropped_filedir);
				editedFrameDurationOld = 999;
				editedFrameIndexOld = 999;
				break;
			}
			else
			{
				printf("\nIncorrect Fileformat\n");
				break;
			}

			// Shows directory of dropped file

			SDL_Surface *droppedImage = NULL;
			droppedImage = IMG_Load(dropped_filedir);

			if (droppedImage != NULL)
			{
				int lastindex = 0;
				int loop = strlen(dropped_filedir);

#ifdef __linux__
				for (int i = 0; i < loop; ++i)
				{
					if (dropped_filedir[i] == '/')
					{
						lastindex = i + 1;
					}
				}
#elif defined(_WIN32)
				for (int i = 0; i < loop; ++i)
				{
					if (dropped_filedir[i] == '\\')
					{
						lastindex = i + 1;
					}
				}
#endif

				const int size = loop - lastindex;

				char* filename = "";

				int n = 0;

				for (int i = lastindex; n < size; ++n, ++i)
				{
					filename = append_char(filename, dropped_filedir[i]);
				}

				auto map_val = texture_map.find(filename);
				int tex_index = -1;

				if (map_val != texture_map.end())
				{
					tex_index = map_val->second;
				}

				if (tex_index <= -1)
				{
					texture_map[filename] = used_textures_index;
					tex_index = used_textures_index;

					SDL_Texture *droppedTexture = SDL_CreateTextureFromSurface(renderer, droppedImage);
					used_textures[used_textures_index] = droppedTexture;

					used_textures_index++;
				}

				SDL_FreeSurface(droppedImage);

				AnimFrame *frame = isometricAnim[curClip]->GetFrameAt(curFrame);

				frame->texture = used_textures[tex_index];
				frame->filename = filename;

				int txtW, txtH;
				SDL_QueryTexture(used_textures[tex_index], NULL, NULL, &txtW, &txtH);

				if (frame->sourceRect.w <= 0) frame->sourceRect.w = txtW / 2;
				if (frame->sourceRect.h <= 0) frame->sourceRect.h = txtH / 2;

				for (int i = 0; i < 8; ++i)
				{
					AnimFrame *otherFrame = isometricAnim[i]->GetFrameAt(0);
					if (otherFrame->texture == used_textures[0])
					{
						otherFrame->texture = used_textures[tex_index];
						otherFrame->filename = filename;

						if (otherFrame->sourceRect.w <= 0) otherFrame->sourceRect.w = txtW / 2;
						if (otherFrame->sourceRect.h <= 0) otherFrame->sourceRect.h = txtH / 2;
					}
				}
			}

			//printf("DROPPED %s", dropped_filedir);

			SDL_free(dropped_filedir);    // Free dropped_filedir memory
			break;

		}

		if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT))
		{
			if (SelectingSource && ReleasedAfterStartingSelection)
			{
				if (!PlacingRect)
				{
					PlacedShit = true;
					SDL_GetMouseState(&startX, &startY);
					MouseRect.x = startX; MouseRect.y = startY;
					MouseRect.w = 0; MouseRect.h = 0;
					PlacingRect = true;
				}

				PlaceMouseRect();
			}
		}
		else if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_RIGHT))
		{
			if (SelectingSource)
			{
				int mouseX = 0; int mouseY = 0;
				SDL_GetMouseState(&mouseX, &mouseY);

				if (select_off_anchor_x == 0)
					select_off_anchor_x = mouseX - select_off_x;

				if (select_off_anchor_y == 0)
					select_off_anchor_y = mouseY - select_off_y;

				select_off_x = mouseX - select_off_anchor_x;
				select_off_y = mouseY - select_off_anchor_y;

				MouseRect.x = 1601;
			}
		}
		else
		{
			int mouseX = 0; int mouseY = 0;
			SDL_GetMouseState(&mouseX, &mouseY);

			if (mouseX <= 1150)
			{
				ReleasedAfterStartingSelection = true;
			}


			if (PlacingRect && PlacedShit)
			{
				SelectingSource = false;
				PlacingRect = false;

				int texw, texh;
				
				AnimFrame *frame = isometricAnim[curClip]->GetFrameAt(curFrame);

				SDL_QueryTexture(frame->texture, NULL, NULL, &texw, &texh);

				int sourceX = MouseRect.x - select_off_x,
					sourceY = MouseRect.y - select_off_y,
					sourceW = MouseRect.w,
					sourceH = MouseRect.h;

				if (sourceX < select_off_x)
				{
					sourceW -= sourceX;
					sourceX = 0;
				}

				if (sourceY < select_off_anchor_y)
				{
					sourceH -= sourceY;
					sourceY = 0;
				}

				if (sourceX > texw)
				{
					sourceX = 0;
					sourceW = texw;
				}
				if (sourceY > texh)
				{
					sourceY = 0;
					sourceH = texh;
				}
				if (sourceX + sourceW > texw)
				{
					sourceW = texw - sourceX;
				}
				if (sourceY + sourceH > texh)
				{
					sourceH = texh - sourceY;
				}

				frame->sourceRect.x = sourceX;
				frame->sourceRect.y = sourceY;
				frame->sourceRect.w = sourceW;
				frame->sourceRect.h = sourceH;

				MouseRect.x = sourceX;
				MouseRect.y = sourceY;
				MouseRect.w = sourceW;
				MouseRect.h = sourceH;
			}
			else
			{
				select_off_anchor_x = select_off_anchor_y = 0;
			}
		}

		if (update)
		{
			for (int i = 0; i < buttonsSize; ++i)
			{
				buttons[i]->Update();
			}

			if (!Saving)
			{
				if (buttons[23]->Pressed())
				{
					curClip++;

					if (curClip > 7)
					{
						curClip = 0;
					}

					isometricAnim[curClip]->SkipTo(0);
					curFrame = 0;
					editedFrameIndexOld = 999;
				}
				else if (buttons[24]->Pressed())
				{
					curClip--;

					if (curClip < 0)
					{
						curClip = 7;
					}

					isometricAnim[curClip]->SkipTo(0);
					curFrame = 0;
					editedFrameIndexOld = 999;
				}

				else if (buttons[8]->Pressed())
				{
					printf("CurFrame: %d\n",curFrame);
					AnimFrame *newFrame = isometricAnim[curClip]->GetFrameAt(curFrame)->Copy();
					printf("Asd\n");
					isometricAnim[curClip]->Add(newFrame, true, curFrame);
					++curFrame;
					printf("Qwe\n");
				}
				else if (buttons[9]->Pressed())
				{
					if (curFrame + 1 >= isometricAnim[curClip]->Size())
					{
						printf("Currently at last frame\n");
					}
					else
					{
						++curFrame;
					}
				}
				else if (buttons[10]->Pressed())
				{
					if (curFrame - 1 < 0)
					{
						printf("Currently at first frame\n");
					}
					else
					{
						--curFrame;
					}
				}
				else if (buttons[11]->Pressed())
				{
					if (isometricAnim[curClip]->MoveToRight(curFrame))
					{
						++curFrame;
						if (curFrame >= isometricAnim[curClip]->Size()) curFrame = isometricAnim[curClip]->Size() - 1;
					}
				}
				else if (buttons[12]->Pressed())
				{
					if (isometricAnim[curClip]->MoveToLeft(curFrame))
					{
						--curFrame;
						if (curFrame < 0) curFrame = 0;
					}
				}

				AnimFrame *frame = isometricAnim[curClip]->GetFrameAt(curFrame);

				if (buttons[0]->Pressed() || buttons[0]->HeldTimer() >= 10)
				{
					frame->sourceRect.y--;
				}
				else if (buttons[1]->Pressed() || buttons[1]->HeldTimer() >= 10)
				{
					frame->sourceRect.y++;
				}
				else if (buttons[2]->Pressed() || buttons[2]->HeldTimer() >= 10)
				{
					frame->sourceRect.x--;
				}
				else if (buttons[3]->Pressed() || buttons[3]->HeldTimer() >= 10)
				{
					frame->sourceRect.x++;
				}

				else if (buttons[4]->Pressed() || buttons[4]->HeldTimer() >= 10)
				{
					frame->sourceRect.h--;
				}
				else if (buttons[5]->Pressed() || buttons[5]->HeldTimer() >= 10)
				{
					frame->sourceRect.h++;
				}
				else if (buttons[6]->Pressed() || buttons[6]->HeldTimer() >= 10)
				{
					frame->sourceRect.w--;
				}
				else if (buttons[7]->Pressed() || buttons[7]->HeldTimer() >= 10)
				{
					frame->sourceRect.w++;
				}
				else if (buttons[13]->Pressed() || buttons[13]->HeldTimer() >= 10)
				{
					frame->offset_y--;
				}
				else if (buttons[14]->Pressed() || buttons[14]->HeldTimer() >= 10)
				{
					frame->offset_y++;
				}
				else if (buttons[15]->Pressed() || buttons[15]->HeldTimer() >= 10)
				{
					frame->offset_x--;
				}
				else if (buttons[16]->Pressed() || buttons[16]->HeldTimer() >= 10)
				{
					frame->offset_x++;
				}
				else if (buttons[26]->Pressed() || buttons[26]->HeldTimer() >= 10)
				{
					for (int i = 0; i < isometricAnim[curClip]->Size(); ++i)
					{ isometricAnim[curClip]->GetFrameAt(i)->offset_y--; }

					editedFrameIndexOld = 999;
				}
				else if (buttons[27]->Pressed() || buttons[27]->HeldTimer() >= 10)
				{
					for (int i = 0; i < isometricAnim[curClip]->Size(); ++i)
					{ isometricAnim[curClip]->GetFrameAt(i)->offset_y++; }

					editedFrameIndexOld = 999;
				}
				else if (buttons[28]->Pressed() || buttons[28]->HeldTimer() >= 10)
				{
					for (int i = 0; i < isometricAnim[curClip]->Size(); ++i)
					{ isometricAnim[curClip]->GetFrameAt(i)->offset_x--; }

					editedFrameIndexOld = 999;
				}
				else if (buttons[29]->Pressed() || buttons[29]->HeldTimer() >= 10)
				{
					for (int i = 0; i < isometricAnim[curClip]->Size(); ++i)
					{ isometricAnim[curClip]->GetFrameAt(i)->offset_x++; }

					editedFrameIndexOld = 999;
				}
				else if (buttons[17]->Pressed() || buttons[17]->HeldTimer() >= 10)
				{
					zoom -= 0.01f;
					if (zoom < 0.5) { zoom = 0.5f; }
				}
				else if (buttons[18]->Pressed() || buttons[18]->HeldTimer() >= 10)
				{
					zoom += 0.01f;
					if (zoom > 10) { zoom = 10; }
				}
				else if (buttons[19]->Pressed() || buttons[19]->HeldTimer() >= 10)
				{
					AnimFrame *frame = isometricAnim[curClip]->GetFrameAt(curFrame);

					frame->frameDuration -= oneframe;

					if (frame->frameDuration <= oneframe)
					{
						frame->frameDuration = oneframe;
					}
				}
				else if (buttons[20]->Pressed() || buttons[20]->HeldTimer() >= 10)
				{
					isometricAnim[curClip]->GetFrameAt(curFrame)->frameDuration += oneframe;
				}
				else if (buttons[22]->Pressed())
				{
					if (isometricAnim[curClip]->Size() > 1)
					{
						isometricAnim[curClip]->RemoveAt(curFrame);

						if (curFrame <= 0)
						{
							curFrame = 0;
						}
						else
						{
							--curFrame;
						}

						editedFrameDurationOld = 999;
						editedFrameIndexOld = 999;
					}
				}
				else if (buttons[25]->Pressed())
				{
					SelectingSource = true;
					select_off_x = select_off_y = 0;
					select_off_anchor_x = select_off_anchor_y = 0;
					PlacedShit = false;
					ReleasedAfterStartingSelection = false;

					AnimFrame *frame = isometricAnim[curClip]->GetFrameAt(curFrame);

					MouseRect.x = frame->sourceRect.x;
					MouseRect.y = frame->sourceRect.y;
					MouseRect.w = frame->sourceRect.w;
					MouseRect.h = frame->sourceRect.h;
				}
			}

			if (buttons[21]->Pressed())
			{
				printf("Saving!\n");
				Saving = !Saving;

				if (Saving)
				{
					SDL_StartTextInput();
				}
				else { SDL_StopTextInput(); }
			}
			else if (buttons[30]->Pressed())
			{
				printf("Mode Changed\n");
				MutanimMode = !MutanimMode;
			}

			if (!Saving)
			{
				KeyboardAdjust();
			}
		}

		if (editedFrameIndexOld != curFrame)
		{
			editedFrameIndexOld = curFrame;

			SDL_FreeSurface(CurrentFrameTextSurface);
			SDL_DestroyTexture(CurrentFrameText);

			std::string text = "Frame: ";
			text += std::to_string(curFrame + 1);
			text += "/";
			text += std::to_string(isometricAnim[curClip]->Size());

			CurrentFrameTextSurface = TTF_RenderText_Solid(Font, text.c_str(), textColor);
			CurrentFrameText = SDL_CreateTextureFromSurface(renderer, CurrentFrameTextSurface);

			int position_x = 890;
			int position_y = 0;
			int width = 240;
			int height = 80;

			int textW = 0, textH = 0;
			SDL_QueryTexture(CurrentFrameText, NULL, NULL, &textW, &textH);

			CurrentFrameTextDest.x = position_x + width / 2 - textW / 2;
			CurrentFrameTextDest.y = position_y + height / 2 - textH / 2;
			CurrentFrameTextDest.w = textW;
			CurrentFrameTextDest.h = textH;



			SDL_FreeSurface(CurClipSurface);
			SDL_DestroyTexture(CurClipTexture);

			text = "Clip: ";
			text += std::to_string(curClip + 1);
			text += "/8";

			CurClipSurface = TTF_RenderText_Solid(Font, text.c_str(), textColor);
			CurClipTexture = SDL_CreateTextureFromSurface(renderer, CurClipSurface);

			position_x = 1360;
			position_y = 690;
			width = 240;
			height = 80;

			textW = textH = 0;
			SDL_QueryTexture(CurClipTexture, NULL, NULL, &textW, &textH);

			CurClipDest.x = position_x + width / 2 - textW / 2;
			CurClipDest.y = position_y + height / 2 - textH / 2;
			CurClipDest.w = textW;
			CurClipDest.h = textH;
		}

		if (editedFrameDurationOld != isometricAnim[curClip]->GetFrameAt(curFrame)->frameDuration)
		{
			editedFrameDurationOld = isometricAnim[curClip]->GetFrameAt(curFrame)->frameDuration;

			SDL_FreeSurface(CurrentFrameDurSurface);
			std::string text = "Dur: ";
			text += std::to_string((int)((editedFrameDurationOld / oneframe) + 0.5f));
			text += " frames (60fps)";

			CurrentFrameDurSurface = TTF_RenderText_Solid(Font, text.c_str(), textColor);
			CurrentFrameDur = SDL_CreateTextureFromSurface(renderer, CurrentFrameDurSurface);

			int position_x = 1250;
			int position_y = 500;
			int width = 240;
			int height = 80;

			int textW = 0, textH = 0;
			SDL_QueryTexture(CurrentFrameDur, NULL, NULL, &textW, &textH);

			CurrentFrameDurDest.x = position_x + width / 2 - textW / 2;
			CurrentFrameDurDest.y = position_y + height / 2 - textH / 2;
			CurrentFrameDurDest.w = textW;
			CurrentFrameDurDest.h = textH;
		}

		editedFrameIndexOld = curFrame;

		SDL_SetRenderDrawColor(renderer, 49, 77, 121, 255);
		SDL_RenderClear(renderer);

		SDL_RenderCopy(renderer, CurrentFrameText, NULL, &CurrentFrameTextDest);
		SDL_RenderCopy(renderer, CurrentFrameDur, NULL, &CurrentFrameDurDest);

		SDL_RenderCopy(renderer, CurClipTexture, NULL, &CurClipDest);

		if (!SelectingSource)
		{
			if (MutanimMode)
			{
				SDL_RenderCopy(renderer, MutanimModeTexture, NULL, &MutanimModeDest);
			}
			else
			{
				SDL_RenderCopy(renderer, AnimModeTexture, NULL, &AnimModeDest);
			}

			SDL_Rect tileDest{ viewOffsetX * zoom, viewOffsetY * zoom + 64 * zoom,  64 * zoom, 64 * zoom };
			//SDL_Rect tileDest{ viewOffsetX * zoom, viewOffsetY * zoom + 14 * zoom,  64 * zoom, 64 * zoom };

			SDL_RenderCopy(renderer, tileTex, NULL, &tileDest);

			isometricAnim[curClip]->Draw(renderer, zoom, 400 + viewOffsetX * zoom, viewOffsetY * zoom);

			AnimFrame *frame = isometricAnim[curClip]->GetFrameAt(curFrame);

			SDL_Rect dest{ (frame->offset_x * zoom + viewOffsetX * zoom), (frame->offset_y * zoom + viewOffsetY * zoom), frame->sourceRect.w * zoom, frame->sourceRect.h * zoom };
			SDL_RenderCopy(renderer, frame->texture, &frame->sourceRect, &dest);

			SDL_Rect border = { (frame->offset_x * zoom + viewOffsetX * zoom), (frame->offset_y * zoom + viewOffsetY * zoom), frame->sourceRect.w * zoom, frame->sourceRect.h * zoom };
			SDL_SetRenderDrawColor(renderer, 255, 255, 0, 125);
			SDL_RenderDrawRect(renderer, &border);
		}
		else
		{
			AnimFrame *frame = isometricAnim[curClip]->GetFrameAt(curFrame);

			SDL_SetRenderDrawColor(renderer, 255, 0, 0, 100);
			SDL_RenderFillRect(renderer, &MouseRect);

			int texw = 0, texh = 0;
			SDL_QueryTexture(frame->texture, NULL, NULL, &texw, &texh);

			SDL_Rect dest{ select_off_x, select_off_y, texw, texh };
			SDL_RenderCopy(renderer, frame->texture, NULL, &dest);

			//SDL_Rect border = { (editedFrame->offset_x * zoom + viewOffsetX * zoom), (editedFrame->offset_y * zoom + viewOffsetY * zoom), editedFrame->sourceRect.w * zoom, editedFrame->sourceRect.h * zoom };
			SDL_SetRenderDrawColor(renderer, 255, 255, 0, 125);
			SDL_RenderDrawRect(renderer, &dest);

		}

		for (int i = 0; i < buttonsSize; ++i)
		{
			buttons[i]->Render(renderer);
		}

		if (Saving)
		{
			if (InputTextTexture != NULL) SDL_DestroyTexture(InputTextTexture);
			//printf("%s\n",InputText.c_str());
			InputTextSurface = TTF_RenderText_Solid(Font, InputText, textColor);
			InputTextTexture = SDL_CreateTextureFromSurface(renderer, InputTextSurface); //now you can convert it into a texture

			if (InputTextSurface != NULL) SDL_FreeSurface(InputTextSurface);

			int size = strlen(InputText);
			int InputTextWidth = 24 * size;

			SDL_Rect InputTextDest{ 800 - InputTextWidth / 2, 450 + 15, InputTextWidth, 30 };
			SDL_RenderCopy(renderer, InputTextTexture, NULL, &InputTextDest);

			SDL_DestroyTexture(InputTextTexture);

			SDL_RenderCopy(renderer, SaveHowToTexture, NULL, &HowToSaveRect);
		}

		SDL_SetRenderDrawColor(renderer, 49, 77, 121, 255);

		SDL_RenderCopy(renderer, memetexture, NULL, &memerect);

		//SDL_FillRect();

		SDL_RenderPresent(renderer); // copy to screen
	}

	/* On Linux, this crashes the program. How ever, explicit freeing is not
	* needed anyway at shutdown. // Kuvis */
#if 0
	for (int i = 0; i < buttonsSize; ++i)
	{
		buttons[i]->Free();
	}
#endif

	SDL_FreeSurface(SaveHowToSurface);
	SDL_DestroyTexture(SaveHowToTexture);
	SDL_FreeSurface(CurrentFrameTextSurface);
	SDL_FreeSurface(CurrentFrameDurSurface);
	SDL_DestroyTexture(texture);
	SDL_FreeSurface(image);
	SDL_DestroyTexture(tileTex);
	SDL_DestroyRenderer(renderer);
	TTF_CloseFont(Font);
	TTF_Quit();
	IMG_Quit();
	//SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}

int SaveAnimation()
{
	FILE * fp;

	char* FileToSave = InputText;
	char* AnimationName = FileToSave;


	if (MutanimMode)
	{
		if (!WordContainedIn(".mutanim", FileToSave))
		{
			FileToSave = append_char(FileToSave, '.');
			FileToSave = append_char(FileToSave, 'm');
			FileToSave = append_char(FileToSave, 'u');
			FileToSave = append_char(FileToSave, 't');
			FileToSave = append_char(FileToSave, 'a');
			FileToSave = append_char(FileToSave, 'n');
			FileToSave = append_char(FileToSave, 'i');
			FileToSave = append_char(FileToSave, 'm');
		}
	}
	else
	{
		if (!WordContainedIn(".anim", FileToSave))
		{
			FileToSave = append_char(FileToSave, '.');
			FileToSave = append_char(FileToSave, 'a');
			FileToSave = append_char(FileToSave, 'n');
			FileToSave = append_char(FileToSave, 'i');
			FileToSave = append_char(FileToSave, 'm');
		}
	}

	fp = fopen(FileToSave, "w+");
	//fopen_s(&fp, FileToSave.c_str(), "w+");

	fprintf(fp, "%s\n", AnimationName);

	if (MutanimMode)
	{
		for (int i = 0; i < 8; ++i)
		{
			Animation *ClipToSave = isometricAnim[i];

			fprintf(fp, "%d\n", i);
			fprintf(fp, "%d\n", ClipToSave->Size());

			for (int i = 0; i < ClipToSave->Size(); ++i)
			{
				AnimFrame *frame = ClipToSave->GetFrameAt(i);

				if (frame != NULL)
				{
					fprintf(fp, "%s\n", frame->filename);
					fprintf(fp, "%d %d %d %d %d %d\n", frame->sourceRect.x, frame->sourceRect.y,
						(frame->sourceRect.x + frame->sourceRect.w), (frame->sourceRect.y + frame->sourceRect.h),
						frame->offset_x, frame->offset_y);
					fprintf(fp, "%f\n", frame->frameDuration);
				}
			}
		}
	}
	else
	{
		Animation *ClipToSave = isometricAnim[curClip];

		fprintf(fp, "%d\n", ClipToSave->Size());

		for (int i = 0; i < ClipToSave->Size(); ++i)
		{
			AnimFrame *frame = ClipToSave->GetFrameAt(i);

			if (frame != NULL)
			{
				fprintf(fp, "%s\n", frame->filename);
				fprintf(fp, "%d %d %d %d %d %d\n", frame->sourceRect.x, frame->sourceRect.y,
					(frame->sourceRect.x + frame->sourceRect.w), (frame->sourceRect.y + frame->sourceRect.h),
					frame->offset_x, frame->offset_y);
				fprintf(fp, "%f\n", frame->frameDuration);
			}
		}
	}

	fclose(fp);

	return(0);
}

int LoadAnimation(SDL_Renderer *renderer, char* droppedPath)
{
	int lastindex = 0;
	int loop = strlen(droppedPath);


#ifdef __linux__
	for (int i = 0; i < loop; ++i)
	{
		printf("%c", droppedPath[i]);
		if (droppedPath[i] == '/')
		{
			lastindex = i + 1;
		}
	}
#elif defined(_WIN32)
	for (int i = 0; i < loop; ++i)
	{
		printf("%c", droppedPath[i]);
		if (droppedPath[i] == '\\')
		{
			lastindex = i + 1;
		}
	}
#endif

	printf("\n");

	const int size = loop - lastindex;

	char* filename = "";

	int n = 0;

	for (int i = lastindex; n < size; ++n, ++i)
	{
		printf("%c", droppedPath[i]);
		filename = append_char(filename, droppedPath[i]);
	}

	FileToLoad = droppedPath;

	char* SaveName = "";

	int animnameloop = strlen(filename);

	for (int i = 0; i < animnameloop; ++i)
	{
		if (filename[i] == '.')
		{
			break;
		}

		SaveName = append_char(SaveName, filename[i]);
	}

	InputText = SaveName;

	return LoadAnimation(renderer);
}

int LoadAnim(SDL_Renderer *renderer)
{
	FILE *fp;

	fp = fopen(FileToLoad, "r");

	if (fp == NULL || fp == 0)
	{
		printf("\nThere is no such file as %s\n", FileToLoad);
		return -1;
	}

	char AnimName[99] = { 0 };
	fgets(AnimName, 99, fp);

	for (int i = 0; i < 99; ++i)
	{
		if (AnimName[i] == '\n')
		{
			AnimName[i] = '\0';
			//if (AnimName[i - 1] == ' ')
			//{ AnimName[i - 1] = '\0'; }
			break;
		}
	}

	char line[4096];
	int data_line;

	int frames = -1;

	fgets(line, 4096, fp);
	sscanf(line, "%d", &frames);

	Animation *loadedAnim = new Animation();
	loadedAnim->Initialize();

	for (int i = 0; i < frames; ++i)
	{
		char texturename[99] = { 0 };
		fgets(texturename, 99, fp);

		for (int i = 0; i < 99; ++i)
		{
			if (texturename[i] == '.')
			{ texturename[i + 4] = '\0'; break; }
		}

		int x, y, w, h, offx, offy;

		fgets(line, 4096, fp);
		sscanf(line, "%d %d %d %d %d %d", &x, &y, &w, &h, &offx, &offy);
		w -= x;
		h -= y;

		float dur;

		fgets(line, 4096, fp);
		sscanf(line, "%f", &dur);

		auto map_val = texture_map.find(texturename);
		int tex_index = -1;

		if (map_val != texture_map.end())
		{
			tex_index = map_val->second;
		}

		if (tex_index <= -1)
		{
			SDL_Surface * image = NULL;
			image = IMG_Load(texturename);

			if (image != NULL)
			{
				texture_map[texturename] = used_textures_index;
				tex_index = used_textures_index;

				SDL_Texture *droppedTexture = SDL_CreateTextureFromSurface(renderer, image);
				used_textures[used_textures_index] = droppedTexture;

				used_textures_index++;

				SDL_FreeSurface(image);
			}
		}

		if (tex_index >= 0)
		{
			printf("\nCreating Frame %s %d %d %d %d %d %d %f\n", texturename, x, y, w, h, offx, offy, dur);

			char* actualname = (char*)malloc(strlen(texturename) + 1);

			if (actualname)
			{
				strcpy(actualname, texturename);
			}

			AnimFrame *frame = (AnimFrame*)malloc(sizeof(AnimFrame));
			frame->filename = actualname;
			frame->texture = used_textures[tex_index];
			frame->sourceRect = { x, y, w, h };
			frame->offset_x = offx;
			frame->offset_y = offy;
			frame->frameDuration = dur;
			frame->timer = 0;

			loadedAnim->Add(frame, true);
		}
	}

	if (loadedAnim->Size() > 0)
	{
		isometricAnim[curClip]->Empty();
		free(isometricAnim[curClip]);
		isometricAnim[curClip] = 0;
		isometricAnim[curClip] = loadedAnim;
	}
	else
	{
		loadedAnim->Empty();
		free(loadedAnim);
	}

	curFrame = 0;

	fclose(fp);

	return(0);
}

int LoadMutanim(SDL_Renderer *renderer)
{
	FILE *fp;

	fp = fopen(FileToLoad, "r");

	if (fp == NULL || fp == 0)
	{
		printf("\nThere is no such file as %s\n", FileToLoad);
		return -1;
	}

	char AnimName[99] = { 0 };
	fgets(AnimName, 99, fp);

	for (int i = 0; i < 99; ++i)
	{
		if (AnimName[i] == '\n')
		{
			AnimName[i] = '\0';
			//if (AnimName[i - 1] == ' ')
			//{ AnimName[i - 1] = '\0'; }
			break;
		}
	}

	char line[4096];
	int data_line;

	for (int PowerOfToolDev = 0; PowerOfToolDev < 8; ++PowerOfToolDev)
	{
		printf("\nLoading Clips\n");

		int dir = -1;
		int frames = -1;

		fgets(line, 4096, fp);
		sscanf(line, "%d", &dir);

		if (dir <= -1)
		{
			printf("\nThere is something wrong in the animation...\n");
			break;
		}

		fgets(line, 4096, fp);
		sscanf(line, "%d", &frames);

		Animation *loadedAnim = new Animation();
		loadedAnim->Initialize();

		for (int i = 0; i < frames; ++i)
		{
			char texturename[99] = { 0 };
			fgets(texturename, 99, fp);

			for (int i = 0; i < 99; ++i)
			{
				if (texturename[i] == '.')
				{
					texturename[i + 4] = '\0'; break;
				}
			}

			int x, y, w, h, offx, offy;

			fgets(line, 4096, fp);
			sscanf(line, "%d %d %d %d %d %d", &x, &y, &w, &h, &offx, &offy);
			w -= x;
			h -= y;

			float dur;

			fgets(line, 4096, fp);
			sscanf(line, "%f", &dur);

			auto map_val = texture_map.find(texturename);
			int tex_index = -1;

			if (map_val != texture_map.end())
			{
				tex_index = map_val->second;
			}

			if (tex_index <= -1)
			{
				SDL_Surface * image = NULL;
				image = IMG_Load(texturename);

				if (image != NULL)
				{
					texture_map[texturename] = used_textures_index;
					tex_index = used_textures_index;

					SDL_Texture *droppedTexture = SDL_CreateTextureFromSurface(renderer, image);
					used_textures[used_textures_index] = droppedTexture;

					used_textures_index++;

					SDL_FreeSurface(image);
				}
			}

			if (tex_index >= 0)
			{
				printf("\nCreating Frame %s %d %d %d %d %d %d %f\n", texturename, x, y, w, h, offx, offy, dur);

				char* actualname = (char*)malloc(strlen(texturename) + 1);

				if (actualname)
				{
					strcpy(actualname, texturename);
				}

				AnimFrame *frame = (AnimFrame*)malloc(sizeof(AnimFrame));
				frame->filename = actualname;
				frame->texture = used_textures[tex_index];
				frame->sourceRect = { x, y, w, h };
				frame->offset_x = offx;
				frame->offset_y = offy;
				frame->frameDuration = dur;
				frame->timer = 0;

				loadedAnim->Add(frame, true);
			}

			//AnimFrame af = new AnimFrame(texturename, );
		}

		if (loadedAnim->Size() > 0)
		{
			if (dir >= 0 && dir <= 7) 
			{ 
				isometricAnim[dir]->Empty();
				free(isometricAnim[dir]);
				isometricAnim[dir] = 0;
				isometricAnim[dir] = loadedAnim; 
			}
		}
		else
		{
			loadedAnim->Empty();
			free(loadedAnim);
		}
	}

	curFrame = 0;
	curClip = 0;

	fclose(fp);

	return(0);
}

int LoadAnimation(SDL_Renderer *renderer)
{
	if (WordContainedIn(".mutanim", FileToLoad))
	{
		MutanimMode = true; 
		return LoadMutanim(renderer);
	}

	MutanimMode = false;
	return LoadAnim(renderer);
}
