/*
MUTAnimator is simple program for creating animations, developed for MUTA.
Copyright (C) 2017 Jerba

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

struct AnimFrame
{
public:
	SDL_Texture *texture;
	SDL_Rect sourceRect;
	char* filename;

	float frameDuration = 0.0166667f * 6;
	float timer = 0;

	int offset_x, offset_y;

	bool operator==(const AnimFrame &other) const
	{
		return
			(texture == other.texture &&
				sourceRect.x == other.sourceRect.x &&
				sourceRect.y == other.sourceRect.y &&
				sourceRect.w == other.sourceRect.w &&
				sourceRect.h == other.sourceRect.h &&
				frameDuration == other.frameDuration);
	}

	void Reset()
	{
		timer = 0;
	}

	AnimFrame *Copy()
	{
		AnimFrame *copy = (AnimFrame*)malloc(sizeof(AnimFrame));
		copy->filename = filename;
		copy->texture = texture;
		copy->sourceRect = { sourceRect.x, sourceRect.y, sourceRect.w, sourceRect.h };
		copy->offset_x = offset_x;
		copy->offset_y = offset_y;
		copy->frameDuration = frameDuration;
		copy->timer = 0;

		return copy;
	}
};

struct  Animation
{
private:
	std::vector<AnimFrame*> frames;
	int frames_size;
	int current_frame;

public:

	void Initialize()
	{
		current_frame = 0;
		frames_size = 0;
	}

	void SkipTo(int index)
	{
		if (index <= 0 || index >= frames_size)
		{
			return;
		}

		frames[current_frame]->Reset();

		current_frame = index;
	}

	Animation copy()
	{
		Animation new_group;

		for (int i = 0; i < frames_size; ++i)
		{
			new_group.Add(frames[i]);
		}

		return new_group;
	}

	std::vector<AnimFrame*> GetSprites()
	{
		return frames;
	}

	void Add(AnimFrame *frame, bool ForceAdd = false, int index = -1)
	{
		if (!Has(*frame) || ForceAdd)
		{
			int olsize = frames_size;

			if (index <= -1)
			{
				frames.push_back(frame);
			}
			else
			{
				frames.insert(frames.begin() + index, frame);
			}

			frames_size = frames.size();

			//printf("Sumthing fucky %d/%d\n", olsize, frames_size);
		}
		else
		{
			printf("Animation has identical frame, ignoring add\n");
		}
	}

	bool MoveToRight(int index)
	{
		if (index < 0 || index >= frames_size)
		{
			return false;
		}

		std::rotate(frames.begin() + index, frames.begin() + index + 1, frames.end());

		++index;
		if (index >= frames_size)
		{
			return false;
		}

		int loop = frames_size - 1 - index;
		printf("Gotta loop %d", loop);


		for (int i = 0; i < loop; ++i)
		{
			std::rotate(frames.begin() + index, frames.begin() + index + 1, frames.end());
		}

		return true;
		//std::rotate(frames.begin(), frames.begin() + 1, frames.end());
	}

	bool MoveToLeft(int index)
	{
		printf("Moving Left!\n");

		if (index < 0 || index >= frames_size)
		{
			return false;
		}

		printf("Index is fine\n");

		bool adjusted = false;

		if (index - 1 > 0)
		{
			printf("Move....\n");
			adjusted = true;
			std::rotate(frames.begin(), frames.begin() + (index - 1), frames.begin() + (index - 1) + 1);
		}

		std::rotate(frames.begin(), frames.begin() + index, frames.begin() + index + 1);

		--index;
		if (index < 0)
		{
			return false;
		}

		int loop = index; if (adjusted) { ++index; }

		for (int i = loop; i > 0; --i)
		{
			std::rotate(frames.begin(), frames.begin() + index, frames.begin() + index + 1);
		}

		//printf("Moved object isn't first, ordering shit\n");

		//int loop = index;

		//for (int i = loop; i >= 0; --i)
		//{ std::rotate(frames.begin(), frames.begin() + index, frames.begin() + index + 1); }

		return true;
	}

	AnimFrame* GetFrameAt(int index)
	{
		if (index >= 0 && index < frames_size)
		{
			return frames[index];
		}
	}

	void Remove(AnimFrame frame)
	{
		for (int i = 0; i < frames_size; ++i)
		{
			if (frame == *frames[i])
			{
				frames.erase(frames.begin() + i);
			}
		}

		frames_size = frames.size();
	}

	void RemoveAt(int index)
	{
		if (index < Size())
		{
			free(frames[index]);
			frames.erase(frames.begin() + index);
			frames_size = frames.size();
		}

		current_frame = 0;
	}

	bool Has(AnimFrame frame)
	{
		for (int i = 0; i < frames_size; ++i)
		{
			if (frame == *frames[i])
			{
				return true;
			}
		}

		return false;
	}

	void Empty()
	{
		if (frames_size > 0)
		{
			for (int i = 0; i < frames_size; ++i)
			{
				free(frames[i]);
			}
		}

		frames.clear();
		frames_size = frames.size();
	}

	int Size()
	{
		return frames_size;
	}

	void Update(float deltaTime)
	{
		if (!frames.empty())
		{
			frames[current_frame]->timer += deltaTime;

			if (frames[current_frame]->timer >= frames[current_frame]->frameDuration)
			{
				frames[current_frame]->Reset();
				++current_frame;

				if (current_frame >= frames_size)
				{
					current_frame = 0;
				}
			}
		}

		//printf("Cur/Size %d/%d\n", current_frame, frames_size);
	}

	void Draw(SDL_Renderer *renderer, float zoom, int offsetX = 0, int offsetY = 0)
	{
		if (!frames.empty())
		{
			AnimFrame *frame = frames[current_frame];
			SDL_Rect dest{ (frame->offset_x * zoom) + offsetX, (frame->offset_y * zoom) + offsetY, frame->sourceRect.w * zoom, frame->sourceRect.h * zoom };
			SDL_RenderCopy(renderer, frame->texture, &frame->sourceRect, &dest);
		}
	}
};
